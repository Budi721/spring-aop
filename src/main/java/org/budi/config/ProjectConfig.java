package org.budi.config;

import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(basePackages = {
        "org.budi.services",
        "org.budi.aspects",
})
@EnableAspectJAutoProxy
public class ProjectConfig {


}
