package org.budi.main;

import org.budi.config.ProjectConfig;
import org.budi.services.HelloService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main( String[] args ) {
        try (var c = new AnnotationConfigApplicationContext(ProjectConfig.class)){
            HelloService service = c.getBean(HelloService.class);
            String message = service.hello("Budi");
            System.out.println("Result is : " + message);
        }
    }
}
