package org.budi.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * Pointcut expression execution(* PACKAGE.*.*(..))
 *
 * @Before("execution(* io.datajek.springaop.movierecommenderaop..*.*Filtering(..)) || execution(String io.datajek.springaop.movierecommenderaop..*.*(..))")
 * @Before("execution(String io.datajek.springaop.movierecommenderaop..*.*(String))")
 * @Before("execution(* io.datajek.springaop.movierecommenderaop..*.*Filtering(..))")
 * @Before("execution(String io.datajek.springaop.movierecommenderaop..*.*(..))")
 * @Before("execution(* io.datajek.springaop.movierecommenderaop.business.*.*(..))")
 *
 *    @Pointcut("execution(* io.datajek.springaop.movierecommenderaop.data.*.*(..))")
 * 	public void dataLayerPointcut() {}
 *
 *    @Pointcut("execution(* io.datajek.springaop.movierecommenderaop.business.*.*(..))")
 * 	public void businessLayerPointcut() {}
 *
 * 	to intercept method calls for both layers:
 *    @Pointcut("io.datajek.springaop.movierecommenderaop.aspect.JoinPointConfig.dataLayerPointcut() || "
 * 			+ "io.datajek.springaop.movierecommenderaop.aspect.JoinPointConfig.businessLayerPointcut()")
 * 	public void allLayersPointcut() {}
 *
 * 	for a particular bean
 *    @Pointcut("bean(movie*)")
 *    public void movieBeanPointcut() {}
 *
 * 	custom aspect annotation
 *    @Pointcut("@annotation(io.datajek.springaop.movierecommenderaop.aspect.MeasureTime)")
 *    public void measureTimeAnnotation() {}
 *
 *
 * The first * in the expression corresponds to the return type. * means any return type.
 * Then comes the package name followed by class and method names.
 * The first * after package means any class and the second * means any method. Instead of *, we could specify the class name and method name to make the pointcut expression specific.
 * Lastly, parentheses correspond to arguments. (…) means any kind of argument.
 */

@Aspect
@Component
public class HelloServiceAspect {

    //    @Before("execution(* org.budi.services.HelloService.hello(..))")
    //    public void before() {
    //        System.out.println("A");
    //    }
    //
    //    @After("execution(* org.budi.services.HelloService.hello(..))")
    //    public void after() {
    //        System.out.println("B");
    //    }
    //
    //    @AfterReturning("execution(* org.budi.services.HelloService.hello(..))")
    //    public void afterReturning() {
    //        System.out.println("C");
    //    }
    //
    //    @AfterThrowing("execution(* org.budi.services.HelloService.hello(..))")
    //    public void afterThrowing() {
    //        System.out.println("D");
    //    }

    @Around("execution(* org.budi.services.HelloService.hello(..))")
    public Object around(ProceedingJoinPoint joinPoint) {
        System.out.println("E");
        Object result;
        try {
            result = joinPoint.proceed();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}
